// Express Setup

const express = require('express')

// Mongoose is a package that allows creation of schemas to model our data structure
const mongoose = require('mongoose')

const app = express()
const port = 3001;

// [Section] - Mongoose Connection
// Mongoose uses the "connect" function to connect to the cluster in our MongoDB Atlas

/*
	It takes 2 arguments:
		1. Connection string from our MongoDB Atlas.
		2. Object that contains the middleware/standards that MongoDB uses.
*/
mongoose.connect(`mongodb+srv://jeaneraya:admin123@cluster0.ehvmi0d.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	// (newUrlParser: true) - it allows us to avoid any current and/or future errors while connecting to MongoDB
	useNewUrlParser: true,
	// useUnifiedTopology is "false" by default. Set to true to opt in to using the MongoDB driver's new connection management engine
	useUnifiedTopology: true
})

// Initializes the mongoose connection to the MongoDB Db by assigning "mongoose.connection" to the "db" variable
let db = mongoose.connection


// Listen to the events of the connection by using the "on()" function of the mongoose connection and logs the details in the console based of the event (error or successful)
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", () => console.log("Connected to MongoDB!"))

// [Section] Mongoose Schema

// CREATING A SCHEMA
const taskSchema = new mongoose.Schema({
	// Define the fields with their corresponding data types
	// For a task, it needs a "task name" and its data type will be "String"
	name: String,
	// There is a field called "status" that has a data type of "String" and the default values is "pending" 
	status: {
		type: String,
		default: 'Pending'
	}
})

// MODELS
// The variable/object "Task" can now be used to run commands for interacting with our database.
// "Task" is capitalized following the MVC approach for the naming conventions.
// Models must be in singular form and capitalized
// The first parameter is used to specify the Name of the collection where we will store our data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in our MongoDb collection/s
const Task = mongoose.model('Task', taskSchema);


// CREATING THE ROUTES

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create a user route
app.post('/tasks', (req, res) => {
	// business logic
	Task.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found")
		} else {
			let newTask = new Task({
				name: req.body.anme
			})

			newTask.save((error, savedTask) => {
				if(error) {
					return console.error(error)
				}else{
					return res.status(201).send("New Task Created!")
				}
			})
		}
	})
})

// Get all users from collection - get all users route
app.get('/tasks', (req, res) => {
	Task.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				tasks: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))
